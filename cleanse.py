import sys
import re
import csv

def csv_reader(f):
	csvreader = csv.reader(f)
	read_list = []
	for row in csvreader:
		row = filter(None, row)
		read_list.append(list(row))
	return read_list

def return_pos(tweet, pass_token):
	split = tweet.split(' ')
	new_list = []
	for i in range(0, len(split)):
		if('http' in pass_token):
			if('http' not in split[i]):
				new_list.append(split[i])
		elif('@' in pass_token):
			if('@' not in split[i]):
				new_list.append(split[i])
	new_list = ' '.join(new_list)
	return new_list
	
def remove_URL(read_list):
	pass_token = 'http'
	removeURL = []
	for i in read_list:
		if('http' in i[1]):
			new_list = return_pos(i[1], pass_token)
			temp = []
			temp.append(i[0])
			temp.append(new_list)
			removeURL.append(temp)
		else:
			removeURL.append(i)
	return removeURL

def remove_usernames(removeURL):
	pass_token = '@'
	removeusernames = []
	for i in removeURL:
		if('@' in i[1]):
			new_list = return_pos(i[1], pass_token)
			temp = []
			temp.append(i[0])
			temp.append(new_list)
			removeusernames.append(temp)
		else:
			removeusernames.append(i)
	return removeusernames

def remove_special_chars(removeusernames):
	remove_chars = []
	for i in removeusernames:
		new = []
		new.append(i[0])
		new.append(re.sub('[^A-Za-z]+', ' ', i[1]))
		remove_chars.append(new)
	return remove_chars

def remove_not_available_text(remove_chars):
	removetext = []
	for i in remove_chars:
		if('Not Available' not in i[1]):
			temp = []
			temp.append(i[0])
			temp.append(i[1])
			removetext.append(temp)
	return removetext	

def read_csv_file(fname = "train.csv"):
	with open(fname, 'r') as f:
		temp_read_list = csv_reader(f)
	return temp_read_list
	#print(read_list)

def cleanse_input(read_list):
	#remove URLs
	removeURL = remove_URL(read_list)
	#remove usernames
	removeusernames = remove_usernames(removeURL)
	#remove special characters and numbers
	remove_chars = remove_special_chars(removeusernames)
	#remove Not available text
	removetext = remove_not_available_text(remove_chars)
	return removetext

if __name__ == '__main__':
	# Usage :
	# python3 cleanse.py [file name]
	if len(sys.argv) > 1:
		read_list = read_csv_file(sys.argv[1])
	else:
		read_list = read_csv_file()

	#remove URLs
	removeURL = remove_URL(read_list)
	#print(removeURL)

	#remove usernames
	removeusernames = remove_usernames(removeURL)
	#print(removeusernames)

	#remove special characters and numbers
	remove_chars = remove_special_chars(removeusernames)
	#print(remove_chars)

	#remove Not available text
	removetext = remove_not_available_text(remove_chars)
	print(removetext) #this is a list of list -> [[label1, tweet1], [label2, tweet2], ...]


