import sys
import string
import re
from collections import Counter
import nltk
from cleanse import read_csv_file, cleanse_input
from spelling import correction

''' Spell Checker '''
def words(text): 
	return re.findall(r'\w+', text.lower())

WORDS = Counter(words(open('big.txt').read()))

def P(word, N=sum(WORDS.values())): 
    # "Probability of `word`."
    return WORDS[word] / N

def correction(word): 
    # "Most probable spelling correction for word."
    return max(candidates(word), key=P)

def candidates(word): 
    # "Generate possible spelling corrections for word."
    return (known([word]) or known(edits1(word)) or known(edits2(word)) or [word])

def known(words): 
    # "The subset of `words` that appear in the dictionary of WORDS."
    return set(w for w in words if w in WORDS)

def edits1(word):
    # "All edits that are one edit away from `word`."
    letters    = 'abcdefghijklmnopqrstuvwxyz'
    splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    deletes    = [L + R[1:]               for L, R in splits if R]
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
    replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    inserts    = [L + c + R               for L, R in splits for c in letters]
    return set(deletes + transposes + replaces + inserts)

def edits2(word): 
    "All edits that are two edits away from `word`."
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))
'''
	Source - Peter Norvig's spell corrector
'''


def tokeniser(inp_tweet):
	'''
		callback function 
	'''
	stemmer = nltk.stem.snowball.SnowballStemmer("english", ignore_stopwords = True)
	translator = str.maketrans('', '', string.punctuation)
	text = nltk.wordpunct_tokenize(inp_tweet)
	text = list(map(lambda s: s.translate(translator), text))
	text = list(map(lambda x: x.lower(),filter(lambda x: x.isalnum() , text)))
	# integrate spell checker before stemming
	text = list(map(correction, text))
	text = list(map(stemmer.stem, text))
	print(text)
	return text

def preprocess(preprocess_input):
	'''
		1. Tokenize
		2. Transform to lowercase
		3. Stem
		4. Peter Norvig Spelling Corrector

		Above steps are performed by the tokeniser()
		Each tweet is parsed one at a time, tokeniser() is the call back
	'''
	# list of only labels of the tweet
	labels = list(map(lambda x: x[0], preprocess_input))
	
	# list of tweets as a list of tokens
	tokenised_text = list(map(lambda x: tokeniser(x[1]), preprocess_input))
	
	# return separetely if required by the classifier
	return list(zip(labels, tokenised_text))

if __name__ == '__main__':
	# Usage :
	# python3 cleanse.py [file name]
	if len(sys.argv) > 1:
		read_list = read_csv_file(sys.argv[1])
	else:
		read_list = read_csv_file()

	preprocess_input = cleanse_input(read_list)
	preprocess_output = preprocess(preprocess_input)